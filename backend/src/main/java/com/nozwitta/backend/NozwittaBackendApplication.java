package com.nozwitta.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NozwittaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(NozwittaBackendApplication.class, args);
	}

}
